# Foreign Language Text Reader (FLTR)

FLTR is a human-language training aide.

This repository exists to preserve code removed by the original authors from SourceForge on 2020-09-19. It is believed to contain the most recently available code and documentation.

Archives of the latest release .zip archives and documentation from SourceForge are available from @language_learning_guy
- https://gitlab.com/language_learning_guy/foreign_language_text_reader

If you would like to take over development, please either fork this repository or contact me for access privileges. Unless or until a new author/maintainer comes forward, I would request someone to please make a PR with build instructions as I am not a programmer and am clueless where to begin.

For commercial alternatives with active support communities, see [LingQ](https://lingq.com) and [ReadLang](https://readlang.com).
